# HTTP PUT

http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.6

"The fundamental difference between the POST and PUT requests is reflected in
the different meaning of the Request-URI. The URI in a POST request identifies
the resource that will handle the enclosed entity. That resource might be a
data-accepting process, a gateway to some other protocol, or a separate entity
that accepts annotations. In contrast, the URI in a PUT request identifies the
entity enclosed with the request -- the user agent knows what URI is intended
and the server MUST NOT attempt to apply the request to some other resource. If
the server desires that the request be applied to a different URI, it MUST send
a 301 (Moved Permanently) response; the user agent MAY then make its own
decision regarding whether or not to redirect the request."

What this means is the HTTP RFC does not allow us to identify multipart/form-
data in a PUT request.

However, there are some simple steps to accomplishing what you want even though
it would be in violation of the HTTP specification. Namely they would be:

1) You can detect whether or not PUT was the REQUEST method via
$_SERVER['REQUEST_METHOD'] for example (depends on your SAPI).
2) You may then proceed to process the 'php://input' stream through something
like file_get_contents('php://input'); - as an example.
3) You may then proceed to break up the multipart form data according to its
defined boundaries in PHP user-space code -- according to the specification at
http://tools.ietf.org/html/rfc2388
4) Finally you may chose to do with the file as you'd like (save, discard,
otherwise).

However, it would make no sense at all to create a new superglobal in PHP called
$_PUT since the HTTP PUT verb does not allow for multi-part/form data to begin
with. So whoever is saying PHP is making it difficult to be RESTful is
misleading.

## Ref:

    * https://bugs.php.net/bug.php?id=55815
