<?php

require_once __DIR__ . '/vendor/autoload.php';

$router = new AltoRouter();
$router->setBasePath('/php-routers/altorouter/match');

$router->map('GET','/', 'app/view/home.php', 'home');
$router->map('GET','/users', array('c' => 'UserController', 'a' => 'ListAction'));
$router->map('GET','/users/[i:id]', 'users#show', 'users_show');
$router->map('PUT','/users/[i:id]', 'app/view/update.php', 'users_put'); // Use application/x-www-form-urlencoded for testing.
$router->map('DELETE','/users/[i:id]', 'app/view/delete.php', 'users_delete'); // Use application/x-www-form-urlencoded for testing.
$router->map('POST','/users', 'app/view/create.php', 'users_create'); // Use application/x-www-form-urlencoded for testing.

// match current request
$match = $router->match();

if ($match) {
    require $match['target'];
}
else {
    header("HTTP/1.0 404 Not Found");
    require 'app/view/404.php';
}
