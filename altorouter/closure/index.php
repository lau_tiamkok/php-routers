<?php

require_once __DIR__ . '/vendor/autoload.php';

$router = new AltoRouter();
$router->setBasePath('/php-routers/altorouter/closure');

// Map homepage.
$router->map( 'GET', '/', function() {
    require __DIR__ . '/app/view/home.php';
}, 'home');

// Use application/x-www-form-urlencoded for testing.
$router->map( 'POST', '/users', function() {
    require __DIR__ . '/app/view/create.php';
}, 'create user');

// Map users details page.
$router->map( 'GET', '/users/[i:id]', function ($id) {
    var_dump($id);
    require __DIR__ . '/app/view/details.php';
}, 'user details');

// Use application/x-www-form-urlencoded for testing.
$router->map( 'PUT', '/users/[i:id]', function ($id) {
    require __DIR__ . '/app/view/update.php';
}, 'update user details');

// Use application/x-www-form-urlencoded for testing.
$router->map( 'DELETE', '/users/[i:id]', function ($id) {
    require __DIR__ . '/app/view/delete.php';
}, 'delete user');

$match = $router->match();

// Call closure or throw 404 status.
// @ ref: http://altorouter.com/usage/processing-requests.html
if($match && is_callable($match['target'])) {
    call_user_func_array($match['target'], $match['params']);
} else {
    // No route was matched.
    header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
    require __DIR__ . '/app/view/404.php';
}
